package com.shumi.plugin_sdk.demo;

import java.util.ArrayList;
import java.util.List;

import com.shumi.plugin_sdk.demo.bean.Authorization;
import com.shumi.plugin_sdk.demo.bean.IFunction;
import com.shumi.plugin_sdk.demo.bean.LocalFunction;
import com.shumi.plugin_sdk.demo.bean.ModifyBindMobile;
import com.shumi.plugin_sdk.demo.bean.ModifyTradePass;
import com.shumi.plugin_sdk.demo.bean.ResetTradePass;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataManager;
import com.shumi.plugin_sdk.demo.fragment.AvaiableFundFragment;
import com.shumi.plugin_sdk.demo.fragment.ManageBankCardsFragment;
import com.shumi.plugin_sdk.demo.fragment.MyFundsharesFragment;
import com.shumi.plugin_sdk.demo.fragment.SdkEnvSettingFragment;
import com.shumi.plugin_sdk.demo.fragment.TradeRecordsFragment;

public class ShumiSdkFunctionManager {

	private static final List<IFunction> list = new ArrayList<IFunction>();

	static {
		list.add(new Authorization("账户\n授权", false));
		list.add(new LocalFunction("基金\n列表", false, AvaiableFundFragment.class));
		list.add(new LocalFunction("环境\n配置", false, SdkEnvSettingFragment.class));
		list.add(new LocalFunction("我的\n持仓", true, MyFundsharesFragment.class));
		list.add(new LocalFunction("交易\n记录", true, TradeRecordsFragment.class));
		list.add(new LocalFunction("银行卡\n管理", true, ManageBankCardsFragment.class));
		list.add(new ResetTradePass("重置\n交易密码", false));
		list.add(new ModifyTradePass("修改\n交易密码", true));
		list.add(new ModifyBindMobile("修改\n手机绑定", true));
	}

	public static List<IFunction> get() {
		return list;
	}
	
	public static int size() {
		int size = 0;
		ShumiSdkDemoDataManager data = ShumiSdkDemoDataManager.getInstance();
		for (IFunction func : list) {
			if(!data.isAuthorised() && func.isUserLevel()){
				continue;
			}
			size++;
		}
		return size;
	}

	public static IFunction get(String key) {
		IFunction func = null;
		for (IFunction demo : list) {
			if (key.equals(demo.getName())) {
				func = demo;
				break;
			}
		}
		return func;
	}

}
