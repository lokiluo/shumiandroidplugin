package com.shumi.plugin_sdk.demo;

import android.util.Log;

import com.shumi.sdk.logging.ShumiSdkLogContent;
import com.shumi.sdk.logging.ShumiSdkLogHandler;

/**
 * 日志句柄
 * @author John
 *
 */
public class ShumiSdkDemoLogHandler extends ShumiSdkLogHandler {
	public static ShumiSdkDemoLogHandler Instance = new ShumiSdkDemoLogHandler();
	
	// 默认的日志等级
	private volatile int logPriority = Log.DEBUG;
	
	public int getLogPriority() {
		return logPriority;
	}

	public void setLogPriority(int logPriority) {
		this.logPriority = logPriority;
	}

	@Override
	public void log(ShumiSdkLogContent logContent) {
		// TODO 请根据实际情况进行处理
		if (logContent.getPriority() >= logPriority) {
			Log.println(logContent.getPriority(), logContent.getTag(), logContent.getMessageEx());
		}
	}
}
