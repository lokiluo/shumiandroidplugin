package com.shumi.plugin_sdk.demo.fragment;

import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;
import com.shumi.plugin_sdk.demo.activity.ShumiSdkDemoFundApplyRecordsActivity;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.plugin_sdk.demo.fragment.MyFundsharesFragment.DataAdapter.ViewHolder;
import com.shumi.sdk.data.bean.ShumiSdkCodeMessageBean;
import com.shumi.sdk.data.param.trade.general.ShumiSdkModifyDividendParam;
import com.shumi.sdk.data.param.trade.general.ShumiSdkRedeemParam;
import com.shumi.sdk.data.param.trade.general.ShumiSdkTransformFundParam;
import com.shumi.sdk.data.service.openapi.IShumiSdkOpenApiDataServiceHandler;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeAccountExtraInfoBean;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeFundSharesBean;
import com.shumi.sdk.ext.data.service.ShumiSdkApplyRecordsDataService;
import com.shumi.sdk.ext.data.service.ShumiSdkGetAccountExtraInfoDataService;
import com.shumi.sdk.ext.data.service.ShumiSdkGetFundSharesDataService;
import com.shumi.sdk.ext.data.service.ShumiSdkGetTakeLimitDataService;

public class MyFundsharesFragment extends Fragment implements
		IShumiSdkOpenApiDataServiceHandler {

	private static final String LogTag = "MyFundsharesFragment";

	private ListView mLvData;
	private DataAdapter mDataAdapter;
	private TextView mTvAccountExtraInfo;

	private ShumiSdkDemoDataBridge mDataBridge;

	// 查询持仓请求
	private ShumiSdkGetFundSharesDataService mFundSharesDataService;
	// 快速赎回限制请求
	private ShumiSdkGetTakeLimitDataService mTakeLimitDataService;
	// 帐户附加信息（在途份额）
	private ShumiSdkGetAccountExtraInfoDataService mAccountExtraInfoDataService;

	private CountDownLatch mCountDownLatch = new CountDownLatch(3);
	private ThreadPoolExecutor mThreadPool = (ThreadPoolExecutor) Executors
			.newCachedThreadPool();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		initDataService();
	}

	// 初始化请求
	public void initDataService() {

		mDataBridge = ShumiSdkDemoDataBridge.getInstance(getActivity());

		mFundSharesDataService = new ShumiSdkGetFundSharesDataService(
				getActivity(), mDataBridge, false);
		mFundSharesDataService.setDataServiceCallback(this);

		mTakeLimitDataService = new ShumiSdkGetTakeLimitDataService(
				getActivity(), mDataBridge);
		mTakeLimitDataService.setDataServiceCallback(this);

		mAccountExtraInfoDataService = new ShumiSdkGetAccountExtraInfoDataService(
				getActivity(), mDataBridge);
		mAccountExtraInfoDataService.setDataServiceCallback(this);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fundshares, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.refresh) {
			doRequest();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.shumi_sdk_fragment_fund_shares,
				null);
		mLvData = (ListView) view.findViewById(R.id.listViewData);
		mLvData.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ViewHolder vh = (ViewHolder) arg1.getTag();
				Intent intent = new Intent(getActivity(),
						ShumiSdkDemoFundApplyRecordsActivity.class);
				intent.putExtra(
						ShumiSdkDemoFundApplyRecordsActivity.TAG_EXTRA_FUNDSHARE,
						vh.mDataItem);
				startActivity(intent);

			}
		});
		mDataAdapter = new DataAdapter(getActivity());
		mLvData.setAdapter(mDataAdapter);
		mTvAccountExtraInfo = (TextView) view
				.findViewById(R.id.textViewAccountExtraInfo);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		doRequest();
	}

	// 请求数据
	public void doRequest() {
		
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(true);

		mLvData.setVisibility(View.GONE);
		mTvAccountExtraInfo.setVisibility(View.GONE);

		mCountDownLatch = new CountDownLatch(2);
		mFundSharesDataService.cancel();
		mFundSharesDataService.get(null);

		mTakeLimitDataService.cancel();
		mTakeLimitDataService.get(null);

		mAccountExtraInfoDataService.cancel();
		mAccountExtraInfoDataService.get(null);
		mThreadPool.submit(new Runnable() {

			@Override
			public void run() {
				try {
					mCountDownLatch.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onGetData(Object dataObject, Object sender) {
		
		// TODO 在这里处理请求成功的数据
		if (sender == mFundSharesDataService) {
			((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
			mCountDownLatch.countDown();
			try {
				mDataAdapter.clear();
				ShumiSdkTradeFundSharesBean bean = mFundSharesDataService
						.getData(dataObject);
				for (ShumiSdkTradeFundSharesBean.Item item : bean.mFundShare) {
					mDataAdapter.add(item);
				}
			} catch (Exception e) {
				Log.e(LogTag, e.toString());
			} finally {
				mDataAdapter.notifyDataSetChanged();
				mLvData.setVisibility(View.VISIBLE);
			}
		} else if (sender == mTakeLimitDataService) {
			mCountDownLatch.countDown();
			// TODO 请求测试
			//Toast.makeText(getActivity(), "快速赎回限制数据获取成功", Toast.LENGTH_SHORT)
			//		.show();
		} else if (sender == mAccountExtraInfoDataService) {
			mCountDownLatch.countDown();
			ShumiSdkTradeAccountExtraInfoBean bean = mAccountExtraInfoDataService
					.getData(dataObject);
			// mTvAccountExtraInfo.setVisibility(View.VISIBLE);
			String extraInfo = String.format(Locale.CHINA,
					"在途金额:%.2f 在途份额:%.2f", bean.ApplySum, bean.ApplyShare);
			// mTvAccountExtraInfo.setText();
			ActionBarActivity activity = (ActionBarActivity) getActivity();
			activity.getSupportActionBar().setSubtitle(extraInfo);
		}
	}

	@Override
	public void onGetError(int code, String message, Throwable err,
			Object sender) {
		
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		
		if (sender == mFundSharesDataService) {
			/*
			 * TODO 判断是否是服务器维护,服务端维护时返回的错误代码code:500或502
			 * 并且ShumiSdkCodeMessageBean中，
			 * 维护时返回ShumiSdkCodeMessageBean.Code:99999，
			 * 维护信息返回ShumiSdkCodeMessageBean.Message
			 */
			ShumiSdkCodeMessageBean bean = ShumiSdkApplyRecordsDataService
					.getCodeMessage(message);
			if (!TextUtils.isEmpty(bean.getMessage())) {
				Toast.makeText(getActivity(), bean.getMessage(),
						Toast.LENGTH_LONG).show();
			}
			mCountDownLatch.countDown();
		} else if (sender == mTakeLimitDataService) {
			mCountDownLatch.countDown();
			Toast.makeText(getActivity(), "快速赎回限制数据获取失败", Toast.LENGTH_SHORT)
					.show();
			// TODO 错误处理
		} else if (sender == mAccountExtraInfoDataService) {
			mCountDownLatch.countDown();
			Toast.makeText(getActivity(), "获取帐户额外信息失败", Toast.LENGTH_SHORT)
					.show();
			// TODO 错误处理
		}
	}

	class DataAdapter extends ArrayAdapter<com.shumi.sdk.ext.data.bean.ShumiSdkTradeFundSharesBean.Item> {
		class ViewHolder {
			TextView mTvFundCodeAndName;
			TextView mTvFundMarketValue;
			TextView mTvUsableRemainShare;
			TextView mTvBankCard;
			Button mBtnTransform;
			Button mBtnModify;
			Button mBtnNormalRedeem;
			Button mBtnQuickRedeem;
			ShumiSdkTradeFundSharesBean.Item mDataItem;
		}

		public DataAdapter(Context context) {
			super(context, 0);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			if (convertView == null) {
				viewHolder = new ViewHolder();
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.shumi_sdk_list_item_fund_shares, null);
				viewHolder.mTvFundCodeAndName = (TextView) convertView
						.findViewById(R.id.textViewFundName);
				viewHolder.mTvFundMarketValue = (TextView) convertView
						.findViewById(R.id.textViewMarketValue);
				viewHolder.mTvUsableRemainShare = (TextView) convertView
						.findViewById(R.id.textViewUsableRemainShare);
				viewHolder.mTvBankCard = (TextView) convertView
						.findViewById(R.id.textViewBankCard);
				viewHolder.mBtnNormalRedeem = (Button) convertView
						.findViewById(R.id.buttonNormalRedeem);
				viewHolder.mBtnNormalRedeem
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								ShumiSdkTradeFundSharesBean.Item dataItem = (ShumiSdkTradeFundSharesBean.Item) v
										.getTag();
								ShumiSdkRedeemParam param = new ShumiSdkRedeemParam();
								param.setParam(dataItem.FundCode,
										dataItem.FundName,
										dataItem.TradeAccount,
										dataItem.ShareType,
										dataItem.UsableRemainShare,
										dataItem.BankName, dataItem.BankAccount);
								ShumiSdkTradingHelper.doNormalRedeem(
										getActivity(), param);
							}
						});
				
				viewHolder.mBtnTransform = (Button) convertView
						.findViewById(R.id.buttonTransform);
				viewHolder.mBtnTransform.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						ShumiSdkTradeFundSharesBean.Item item = (ShumiSdkTradeFundSharesBean.Item) v
								.getTag();
						ShumiSdkTransformFundParam param = new ShumiSdkTransformFundParam();
						param.setParam(item.FundCode, item.FundName, item.TradeAccount);
						ShumiSdkTradingHelper.doTransformFund(getActivity(), param);
					}
				});

				viewHolder.mBtnModify = (Button) convertView
						.findViewById(R.id.buttonModify);
				viewHolder.mBtnModify
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								ShumiSdkTradeFundSharesBean.Item item = (ShumiSdkTradeFundSharesBean.Item) v
										.getTag();
								ShumiSdkModifyDividendParam param = new ShumiSdkModifyDividendParam();
								param.setParam(item.FundCode, item.FundName,
										item.ShareType, item.TradeAccount,
										String.valueOf(item.MelonMethod),
										item.BankName, item.BankAccount);
								ShumiSdkTradingHelper.doModifyDividend(
										getActivity(), param);
							}
						});

				viewHolder.mBtnQuickRedeem = (Button) convertView
						.findViewById(R.id.buttonQuickRedeem);
				viewHolder.mBtnQuickRedeem
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								ShumiSdkTradeFundSharesBean.Item dataItem = (ShumiSdkTradeFundSharesBean.Item) v
										.getTag();
								ShumiSdkRedeemParam param = new ShumiSdkRedeemParam();
								param.setParam(dataItem.FundCode,
										dataItem.FundName,
										dataItem.TradeAccount,
										dataItem.ShareType,
										dataItem.UsableRemainShare,
										dataItem.BankName, dataItem.BankAccount);
								ShumiSdkTradingHelper.doQuickRedeem(
										getActivity(), param);
							}
						});
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			ShumiSdkTradeFundSharesBean.Item dataItem = getItem(position);
			viewHolder.mTvFundCodeAndName.setText(String
					.format("[%s]%s - %s", dataItem.FundCode,
							dataItem.FundName, dataItem.FundTypeToCN));
			viewHolder.mTvFundMarketValue.setText(String.format("基金市值：%.2f元",
					dataItem.MarketValue));
			viewHolder.mTvUsableRemainShare.setText(String.format("可用份额：%.2f份",
					dataItem.UsableRemainShare));
			viewHolder.mTvBankCard.setText(String.format("银行卡号：%s %s",
					dataItem.BankName, dataItem.BankAccount));

			viewHolder.mBtnTransform.setTag(dataItem);
			viewHolder.mBtnNormalRedeem.setTag(dataItem);
			viewHolder.mBtnModify.setTag(dataItem);
			viewHolder.mBtnQuickRedeem.setTag(dataItem);

			viewHolder.mBtnQuickRedeem
					.setVisibility(dataItem.RapidRedeem ? View.VISIBLE
							: View.INVISIBLE);
			viewHolder.mDataItem = dataItem;
			return convertView;
		}
	}

}
