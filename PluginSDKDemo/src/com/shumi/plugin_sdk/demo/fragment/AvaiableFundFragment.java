package com.shumi.plugin_sdk.demo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;
import com.shumi.plugin_sdk.demo.data.AvailableFunds;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.plugin_sdk.demo.fragment.AvaiableFundFragment.DataAdapater.ViewHolder;
import com.shumi.sdk.data.param.trade.general.ShumiSdkPurchaseFundParam;
import com.shumi.sdk.data.service.openapi.IShumiSdkOpenApiDataServiceHandler;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeAvailableFundsBean;
import com.shumi.sdk.ext.data.service.ShumiSdkGetAvaiableFundsDataService;
import com.shumi.sdk.ext.util.ShumiSdkFundTradingDictionary;
import com.shumi.sdk.ext.util.ShumiSdkFundTradingDictionary.Dictionary;

public class AvaiableFundFragment extends Fragment implements
		IShumiSdkOpenApiDataServiceHandler, OnItemClickListener {

	private ShumiSdkGetAvaiableFundsDataService mDataService;

	private ListView mLvData;
	private EditText mEtSearch;
	private DataAdapater mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		mDataService = new ShumiSdkGetAvaiableFundsDataService(getActivity(),
				ShumiSdkDemoDataBridge.getInstance(getActivity()));
		mDataService.setDataServiceCallback(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.shumi_sdk_fragment_single_tab,
				null);
		mLvData = (ListView) view.findViewById(R.id.listViewData);
		mEtSearch = (EditText) view.findViewById(R.id.et_search);

		mEtSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				mAdapter.clear();
				if (s.length() == 0) {
					mAdapter.addAll(AvailableFunds.getItem());
				} else {
					mAdapter.addAll(AvailableFunds.getItem(String.valueOf(s)));
				}
				mAdapter.notifyDataSetChanged();
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		mAdapter = new DataAdapater(getActivity());

		if (AvailableFunds.getItem().size() > 0) {
			mAdapter.clear();
			mAdapter.addAll(AvailableFunds.getItem());
		}else{
			mLvData.setVisibility(View.GONE);
		}

		mLvData.setAdapter(mAdapter);
		mLvData.setOnItemClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (AvailableFunds.getItem().size() == 0) {
			doRequest();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.avafunds, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.search) {
			if (mEtSearch.getVisibility() == View.VISIBLE) {
				mEtSearch.setVisibility(View.GONE);
			} else {
				mEtSearch.setVisibility(View.VISIBLE);
			}
			return true;
		} else if (item.getItemId() == R.id.refresh) {
			doRequest();
			return true;
		} else if (item.getItemId() == R.id.monetary) {
			mAdapter.clear();
			mAdapter.addAll(AvailableFunds.getItem(2));
			mAdapter.notifyDataSetChanged();
			return true;
		} else if (item.getItemId() == R.id.allfunds) {
			mAdapter.clear();
			mAdapter.addAll(AvailableFunds.getItem());
			mAdapter.notifyDataSetChanged();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onGetData(Object dataObject, Object sender) {
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		if (sender == mDataService) {
			try {
				mAdapter.clear();
				// 实体转换
				ShumiSdkTradeAvailableFundsBean bean = mDataService
						.getData(dataObject);
				AvailableFunds.setItem(bean.mAvailableFund);
				for (ShumiSdkTradeAvailableFundsBean.Item item : bean.mAvailableFund) {
					mAdapter.add(item);
				}
			} catch (Exception e) {

			} finally {
				mAdapter.notifyDataSetChanged();
				mLvData.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void onGetError(int code, String message, Throwable err,
			Object sender) {
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		if (sender == mDataService) {
			mAdapter.clear();
			mAdapter.notifyDataSetChanged();
		}
	}

	protected void doRequest() {
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(true);
		mDataService.cancel();
		mDataService.get(null);
		// mLvData.setVisibility(View.GONE);
	}

	class DataAdapater extends
			ArrayAdapter<com.shumi.sdk.ext.data.bean.ShumiSdkTradeAvailableFundsBean.Item> {
		class ViewHolder {
			TextView mTvFundName;
			TextView mTvFundInfo;
			TextView mTvFundStatus;
			ShumiSdkTradeAvailableFundsBean.Item mDataItem;
		}

		public DataAdapater(Context context) {
			super(context, 0);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder vh = null;
			if (convertView == null) {
				vh = new ViewHolder();
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.shumi_sdk_list_item_avaiable_fund, null);
				vh.mTvFundName = (TextView) convertView
						.findViewById(R.id.textViewFundName);
				vh.mTvFundInfo = (TextView) convertView
						.findViewById(R.id.textViewFundInfo);
				vh.mTvFundStatus = (TextView) convertView
						.findViewById(R.id.textViewFundStatus);
				convertView.setTag(vh);
			} else {
				vh = (ViewHolder) convertView.getTag();
			}
			ShumiSdkTradeAvailableFundsBean.Item bean = getItem(position);
			vh.mTvFundName.setText(String.format("%s %s", bean.FundCode,
					bean.FundName));
			String fundType = ShumiSdkFundTradingDictionary.getInstance(
					getContext()).lookup(Dictionary.FundType, bean.FundType);
			String buyState = "";
			if (bean.DeclareState) {
				buyState += "可申购";
			} else if (bean.SubscribeState) {
				buyState += "可认购";
			}
			if (bean.ValuagrState) {
				if (!TextUtils.isEmpty(buyState)) {
					buyState += "/";
				}
				buyState += "可普通定投";
			}
			String fundState = ShumiSdkFundTradingDictionary.getInstance(
					getContext()).lookup(Dictionary.FundState, bean.FundState);
			String riskLevel = ShumiSdkFundTradingDictionary.getInstance(
					getContext()).lookup(Dictionary.FundRiskLevel,
					bean.RiskLevel);

			vh.mTvFundInfo.setText(String.format("%s %s", fundType, buyState));
			vh.mTvFundStatus.setText(String.format("%s %s", fundState,
					riskLevel));
			vh.mDataItem = bean;
			return convertView;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (arg0 == mLvData) {
			ViewHolder vh = (ViewHolder) arg1.getTag();
			ShumiSdkTradeAvailableFundsBean.Item bean = vh.mDataItem;
			// 判断基金认申购状态
			if (!bean.SubscribeState && !bean.DeclareState) {
				Toast.makeText(getActivity(), "该基金无法认购或者申购", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			String buyAction = "";
			if (bean.SubscribeState) {
				buyAction = ShumiSdkPurchaseFundParam.ACTION_SUBSCRIBE;
			} else if (bean.DeclareState) {
				buyAction = ShumiSdkPurchaseFundParam.ACTION_PURCHASE;
			}
			try {
				// 设置交易参数
				ShumiSdkPurchaseFundParam param = new ShumiSdkPurchaseFundParam();
				param.setParam(buyAction, bean.FundCode, bean.FundName,
						String.valueOf(bean.RiskLevel), bean.ShareType,
						bean.FundType);
				ShumiSdkTradingHelper.doPurchase(getActivity(), param);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
