package com.shumi.plugin_sdk.demo.fragment;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;

import com.shumi.plugin_sdk.demo.R;

public class LoadingDialogFragment extends DialogFragment {
	private View mContentView;
	private ProgressBar mProgressBar;
	private OnCancelListener onCancelListener;

	public void setOnCancelListener(OnCancelListener listener) {
		this.onCancelListener = listener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(
				R.layout.shumi_sdk_dialog_plugin_update, null);
		getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
		mProgressBar = (ProgressBar) mContentView
				.findViewById(R.id.progressBarUpdate);
		mProgressBar.setProgress(0);
		mProgressBar.setMax(100);
		getDialog().setTitle("");
		getDialog().setCanceledOnTouchOutside(false);
		getDialog().setOnCancelListener(this);
		return mContentView;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		if (onCancelListener != null) {
			onCancelListener.onCancel(dialog);
		} else {
			super.onCancel(dialog);
		}
	}

	public void setProgress(int progress) {
		try {
			mProgressBar.setProgress(progress);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
