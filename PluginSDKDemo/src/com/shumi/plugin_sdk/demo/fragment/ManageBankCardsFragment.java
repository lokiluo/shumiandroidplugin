package com.shumi.plugin_sdk.demo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.sdk.data.param.trade.general.ShumiSdkCardValidationParam;
import com.shumi.sdk.data.param.trade.general.ShumiSdkUnbindBankCardParam;
import com.shumi.sdk.data.service.openapi.IShumiSdkOpenApiDataServiceHandler;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeBindedBankCardBean;
import com.shumi.sdk.ext.data.service.ShumiSdkGetBindBankCardsDataService;

public class ManageBankCardsFragment extends Fragment implements IShumiSdkOpenApiDataServiceHandler{
	
	private ListView mLvData;
	private ShumiSdkGetBindBankCardsDataService mDataService;
	private DataAdapater mAdapter;

	private View mContentView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		initDataService();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.shumi_sdk_fragment_single_tab,
				null);
		mLvData = (ListView) mContentView.findViewById(R.id.listViewData);
		mAdapter = new DataAdapater(getActivity());
		mLvData.setAdapter(mAdapter);
		return mContentView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// 请求数据
		doRequest();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.bankcards, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.refresh) {
			doRequest();
			return true;
		} else if (item.getItemId() == R.id.add_bankcard) {
			ShumiSdkTradingHelper.doAddBankCard(getActivity());
			return true;
		} else if (item.getItemId() == R.id.manage_bankcard) {
			ShumiSdkTradingHelper.doManagementBankCards(getActivity());
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 初始化请求
	 */
	protected void initDataService() {
		// 创建DataService
		mDataService = new ShumiSdkGetBindBankCardsDataService(getActivity(),
				ShumiSdkDemoDataBridge.getInstance(getActivity()));
		// 设置Callback
		mDataService.setDataServiceCallback(this);
	}

	/**
	 * 请求数据
	 */
	protected void doRequest() {
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(true);
		// 取消请求
		mDataService.cancel();
		// 使用get方式请求，传null表示无参
		// 请求成功onGetData/失败onGetError
		mDataService.get(null);
		mLvData.setVisibility(View.GONE);
	}

	@Override
	public void onGetData(Object dataObject, Object sender) {
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		// sender为来源
		if (sender == mDataService) {
			try {
				mAdapter.clear();
				// 实体转换
				ShumiSdkTradeBindedBankCardBean bean = mDataService
						.getData(dataObject);
				for (ShumiSdkTradeBindedBankCardBean.Item item : bean.mBindedBankCard) {
					mAdapter.add(item);
				}
			} catch (Exception e) {

			} finally {
				mAdapter.notifyDataSetChanged();
				mLvData.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void onGetError(int code, String message, Throwable err,
			Object sender) {
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		if (sender == mDataService) {
			mAdapter.clear();
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onPause() {
		mDataService.cancel();
		super.onPause();
	}

	class DataAdapater extends
			ArrayAdapter<ShumiSdkTradeBindedBankCardBean.Item> {
		class ViewHolder {
			TextView mTvCardName;
			TextView mTvCardInfo;
			TextView mTvCardStatus;
			Button mBtnUnbind;
			Button mBtnValidation;
		}

		public DataAdapater(Context context) {
			super(context, 0);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder vh = null;
			if (convertView == null) {
				vh = new ViewHolder();
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.shumi_sdk_list_item_bank_cards, null);
				vh.mTvCardName = (TextView) convertView
						.findViewById(R.id.textViewBankName);
				vh.mTvCardInfo = (TextView) convertView
						.findViewById(R.id.textViewBankInfo);
				vh.mTvCardStatus = (TextView) convertView
						.findViewById(R.id.textViewBankStatus);
				vh.mBtnValidation = (Button) convertView
						.findViewById(R.id.buttonValidation);
				vh.mBtnValidation.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ShumiSdkTradeBindedBankCardBean.Item item = (ShumiSdkTradeBindedBankCardBean.Item) v
								.getTag();
						if (item != null) {
							ShumiSdkCardValidationParam param = new ShumiSdkCardValidationParam();
							param.setBankCard(item.No);
							param.setBankName(item.BankName);
							param.setLimitDescribe(item.LimitDescribe);
							ShumiSdkTradingHelper.doVerifyBankCard(
									getActivity(), param, item.BindWay,
									item.CapitalMode);
						}
					}
				});
				vh.mBtnUnbind = (Button) convertView
						.findViewById(R.id.buttonUnbind);
				vh.mBtnUnbind.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ShumiSdkTradeBindedBankCardBean.Item item = (ShumiSdkTradeBindedBankCardBean.Item) v
								.getTag();
						if (item != null) {
							ShumiSdkUnbindBankCardParam param = new ShumiSdkUnbindBankCardParam();
							param.setParam(item.TradeAccount,
									item.BankName, item.No);
							ShumiSdkTradingHelper.doUnbindBankCard(
									getActivity(), param);
						}
					}
				});
				convertView.setTag(vh);
			} else {
				vh = (ViewHolder) convertView.getTag();
			}
			ShumiSdkTradeBindedBankCardBean.Item bean = getItem(position);
			vh.mBtnValidation.setTag(bean);
			vh.mBtnUnbind.setTag(bean);
			// 未验卡
			if (!bean.IsVaild) {
				// 验证银行卡按钮
				vh.mBtnValidation.setVisibility(View.VISIBLE);
			} else {
				vh.mBtnValidation.setVisibility(View.GONE);
			}
			// 是否已冻结
			if (!bean.IsFreeze) {
				vh.mBtnUnbind.setVisibility(View.VISIBLE);
			} else {
				vh.mBtnUnbind.setVisibility(View.GONE);
			}
			vh.mTvCardName.setText(bean.BankName + " " + bean.No);
			vh.mTvCardInfo.setText(bean.LimitDescribe);
			vh.mTvCardStatus.setText(bean.StatusToCN);
			return convertView;
		}
	}

}
