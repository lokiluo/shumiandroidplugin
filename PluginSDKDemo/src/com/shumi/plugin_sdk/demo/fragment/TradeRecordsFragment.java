package com.shumi.plugin_sdk.demo.fragment;

import java.util.concurrent.atomic.AtomicBoolean;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.plugin_sdk.demo.util.ShumiSdkDemoValueFormator;
import com.shumi.sdk.data.bean.ShumiSdkCodeMessageBean;
import com.shumi.sdk.data.param.trade.general.ShumiSdkCancelOrderParam;
import com.shumi.sdk.data.service.openapi.IShumiSdkOpenApiDataServiceHandler;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeApplyRecordsBean;
import com.shumi.sdk.ext.data.service.ShumiSdkApplyRecordsDataService;
import com.shumi.sdk.ext.util.ShumiSdkFundTradingDictionary;
import com.shumi.sdk.ext.util.ShumiSdkFundTradingDictionary.Dictionary;

public class TradeRecordsFragment extends Fragment implements OnScrollListener,
		IShumiSdkOpenApiDataServiceHandler {

	private View mContentView;
	private static final String LogTag = TradeRecordsFragment.class.getName();
	private ListView mLvData;
	private ApplyRecordsDataAdapter mAdapter;

	// 是否有更多数据
	private AtomicBoolean mHasMoreData = new AtomicBoolean(false);
	// 字典帮助类
	private ShumiSdkFundTradingDictionary mDictionary;
	// 交易记录请求
	private ShumiSdkApplyRecordsDataService mDataService;
	// 交易记录请求参数
	private ShumiSdkApplyRecordsDataService.Param mParam;

	// 默认分页数
	private static final int defaultPageSize = 200;

	// 初始化请求
	private void initDataService() {
		mDataService = new ShumiSdkApplyRecordsDataService(getActivity(),
				ShumiSdkDemoDataBridge.getInstance(getActivity()));
		mParam = new ShumiSdkApplyRecordsDataService.Param();
		mDataService.setDataServiceCallback(this);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	protected View findViewById(int id) {
		return mContentView.findViewById(id);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
		initDataService();
	}

	@Override
	public void onPause() {
		mDataService.cancel();
		super.onPause();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.shumi_sdk_fragment_single_tab,
				null);
		return mContentView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mDictionary = ShumiSdkFundTradingDictionary.getInstance(getActivity());
		mLvData = (ListView) findViewById(R.id.listViewData);
		mAdapter = new ApplyRecordsDataAdapter(getActivity());
		mLvData.setAdapter(mAdapter);
		mLvData.setOnScrollListener(this);
		doRequest();
	}

	private void doRequest() {
		mDataService.cancel();
		
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(true);

		mHasMoreData.set(false);
		mParam.PageIndex = 1;
		mParam.PageSize = defaultPageSize;
		mParam.StartTime = "0001-01-01";
		mParam.EndTime = "9999-01-01";

		// TODO 这里用的POST方式，也可以用GET方式
		// 如果全支持，建议用GET，这里只是演示- -
		mDataService.post(mParam);
		mLvData.setVisibility(View.GONE);
	}

	private void doRequestMore() {
		mDataService.cancel();
		mHasMoreData.set(false);
		mParam.PageIndex += 1;
		mDataService.get(mParam);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fundshares, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.refresh) {
			doRequest();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public class ApplyRecordsDataAdapter extends
			ArrayAdapter<ShumiSdkTradeApplyRecordsBean.Item> {
		class ViewHolder {
			TextView mTvFundCodeAndName;
			TextView mTvPayResult;
			TextView mTvBankAcco;
			TextView mTvApplyTime;
			TextView mTvBusinessType;
			TextView mTvStatus;
			TextView mTvSumAndShare;
			Button mBtnCancelOrder;
		}

		public ApplyRecordsDataAdapter(Context context) {
			super(context, 0);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder vh = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.shumi_sdk_list_item_apply_records, null);
				vh = new ViewHolder();
				vh.mTvFundCodeAndName = (TextView) convertView
						.findViewById(R.id.textViewFundCodeAndName);
				vh.mTvBankAcco = (TextView) convertView
						.findViewById(R.id.textViewBankAcco);
				vh.mTvPayResult = (TextView) convertView
						.findViewById(R.id.textViewPayResult);
				vh.mTvApplyTime = (TextView) convertView
						.findViewById(R.id.textViewApplyTime);
				vh.mTvBusinessType = (TextView) convertView
						.findViewById(R.id.textViewBusinessType);
				vh.mTvStatus = (TextView) convertView
						.findViewById(R.id.textViewStatus);
				vh.mTvSumAndShare = (TextView) convertView
						.findViewById(R.id.textViewApplySumAndShare);
				vh.mBtnCancelOrder = (Button) convertView
						.findViewById(R.id.buttonCancelOrder);
				vh.mBtnCancelOrder.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ShumiSdkTradeApplyRecordsBean.Item item = (ShumiSdkTradeApplyRecordsBean.Item) v
								.getTag();
						ShumiSdkCancelOrderParam param = new ShumiSdkCancelOrderParam();
						param.setParam(item.ApplySerial);
						ShumiSdkTradingHelper.doCancelOrder(getActivity(),
								param);
					}
				});
			} else {
				vh = (ViewHolder) convertView.getTag();
			}
			ShumiSdkTradeApplyRecordsBean.Item item = getItem(position);
			vh.mTvFundCodeAndName.setText(String.format("%s %s", item.FundCode,
					item.FundName));
			String payResult = mDictionary.lookup(Dictionary.PayResult,
					item.PayResult);
			vh.mTvPayResult.setText(payResult);
			vh.mTvPayResult
					.setVisibility(TextUtils.isEmpty(payResult) ? View.GONE
							: View.VISIBLE);
			// 请届时处理好银行卡号的显示(显示后四位)
			vh.mTvBankAcco.setText(String.format("%s %s", item.BankName,
					item.BankAccount));
			vh.mTvApplyTime.setText(ShumiSdkDemoValueFormator
					.reformatDate(item.ApplyDateTime));
			vh.mTvBusinessType.setText(mDictionary.lookup(Dictionary.BusinFlag,
					item.BusinessType));
			vh.mTvStatus.setText(mDictionary.lookup(Dictionary.ConfirmState,
					item.Status));
			vh.mTvSumAndShare.setText(String.format("申请金额:%.2f 申请份额:%.2f",
					item.Amount, item.Shares));
			vh.mBtnCancelOrder.setVisibility(item.CanCancel ? View.VISIBLE
					: View.GONE);
			vh.mBtnCancelOrder.setTag(item);
			convertView.setTag(vh);
			return convertView;
		}
	}

	@Override
	public void onGetData(Object dataObject, Object sender) {
		
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		
		if (mDataService == sender) {
			try {
				ShumiSdkTradeApplyRecordsBean bean = (ShumiSdkTradeApplyRecordsBean) dataObject;
				// 涉及到分页，如果当前是第一页，则清空表
				if (mParam.PageIndex == 1) {
					mAdapter.clear();
				}

				for (ShumiSdkTradeApplyRecordsBean.Item item : bean.Items) {
					mAdapter.add(item);
				}

				// TODO 分页处理，也可根据实际情况进行修改或调整
				// 此DEMO的处理是快拉至底部请求数据
				// 当前页索引 * 每页记录数 小于 总记录数
				if (mParam.PageIndex * mParam.PageSize < bean.Total) {
					// TODO 仍有数据未加载完 此时只需要将mParam中的页数+1重新请求即可
					mHasMoreData.set(true);
				} else {
					mHasMoreData.set(false);
				}
			} catch (Exception e) {
				Log.e(LogTag, e.toString());
			} finally {
				mAdapter.notifyDataSetChanged();
				mLvData.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void onGetError(int code, String message, Throwable err,
			Object sender) {
		
		((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
		
		if (mDataService == sender) {
			/**
			 * TODO 判断是否是服务器维护,服务端维护时返回的错误代码code:500或502
			 * 并且ShumiSdkCodeMessageBean中，
			 * 维护时返回ShumiSdkCodeMessageBean.Code:99999，
			 * 维护信息返回ShumiSdkCodeMessageBean.Message
			 */
			ShumiSdkCodeMessageBean bean = ShumiSdkApplyRecordsDataService
					.getCodeMessage(message);
			if (bean != null && !TextUtils.isEmpty(bean.getMessage())) {
				Toast.makeText(getActivity(), bean.getMessage(),
						Toast.LENGTH_LONG).show();
			} else if (code == 500 || code == 502) {
				Toast.makeText(getActivity(), R.string.server_maintaince,
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		int visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
		// 加载更多数据,快移动到底部(离底部2行以内)自动加载
		if (visibleLastIndex >= totalItemCount - 2 && mHasMoreData.get()) {
			doRequestMore();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}

}
