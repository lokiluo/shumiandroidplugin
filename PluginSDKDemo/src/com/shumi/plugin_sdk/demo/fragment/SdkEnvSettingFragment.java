package com.shumi.plugin_sdk.demo.fragment;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkConsumerConfig;
import com.shumi.plugin_sdk.demo.ShumiSdkConsumerConfig.Consumer;
import com.shumi.plugin_sdk.demo.ShumiSdkEnvSettings;
import com.shumi.sdk.env.ShumiSdkEnv;
import com.shumi.sdk.utils.ShumiSdkAssetsUtil;
import com.shumi.sdk.utils.ShumiSdkClipboardUtil;

public class SdkEnvSettingFragment extends Fragment implements OnClickListener {

	private View mContentView;
	
	private Button mBtnSwitchEnv;
	private Button mBtnSwitchUpdate;
	private Button mBtnDeleteCache;
	private Button mBtnSwitchConsumer;

	private TextView mTvConsumer;
	private TextView mTvUpdateUrl;
	private TextView mTvPluginPath;
	
	private ExecutorService mExecutorService;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mExecutorService = Executors.newSingleThreadExecutor();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		List<Consumer> items = ShumiSdkConsumerConfig.getConsumers();
		for (int i = 0; i < items.size(); i++) {
			menu.add(items.get(i).Name);
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		ShumiSdkConsumerConfig.switchConsumer(getActivity(), String.valueOf(item.getTitle()));
		udpateContentView();
		return true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.shumi_sdk_fragment_settings,
				null);
		mBtnSwitchEnv = (Button) mContentView.findViewById(R.id.btn_switch_env);
		mBtnSwitchUpdate = (Button) mContentView
				.findViewById(R.id.btn_switch_update);
		mBtnDeleteCache = (Button) mContentView
				.findViewById(R.id.btn_delete_cache);
		mBtnSwitchConsumer = (Button) mContentView
				.findViewById(R.id.btn_switch_consumer);
		mTvUpdateUrl = (TextView) mContentView.findViewById(R.id.tv_update_url);
		mTvPluginPath = (TextView) mContentView
				.findViewById(R.id.tv_plugin_path);
		mTvConsumer = (TextView) mContentView
				.findViewById(R.id.tv_consumer);

		mBtnSwitchEnv.setOnClickListener(this);
		mBtnSwitchUpdate.setOnClickListener(this);
		mBtnDeleteCache.setOnClickListener(this);
		mBtnSwitchConsumer.setOnClickListener(this);
		mTvUpdateUrl.setOnClickListener(this);
		mTvPluginPath.setOnClickListener(this);
		mTvConsumer.setOnClickListener(this);
		
		mTvConsumer.setVisibility(View.GONE);
		
		List<Consumer> items = ShumiSdkConsumerConfig.getConsumers();
		if (items.size() == 0) {
			mBtnSwitchConsumer.setVisibility(View.GONE);
			mTvConsumer.setVisibility(View.GONE);
		}else{
			// registerForContextMenu(mTvConsumer);
			registerForContextMenu(mBtnSwitchConsumer);
		}
		
		udpateContentView();
		
		return mContentView;
	}

	@Override
	public void onClick(View v) {
		if (v == mBtnSwitchEnv) {
			ShumiSdkEnvSettings.switchEnv();
		} else if (v == mBtnSwitchUpdate) {
			ShumiSdkEnvSettings.switchUpdateUrl();
		} else if (v == mBtnDeleteCache) {
			ShumiSdkAssetsUtil.clearSDCardAsset();
		} else if (v == mBtnSwitchConsumer) {
			getActivity().openContextMenu(mBtnSwitchConsumer);
		} else if (v == mTvUpdateUrl) {
			String plainText = mTvUpdateUrl.getText().toString().trim();
			ShumiSdkClipboardUtil.copyToClipboard(getActivity(), plainText);
			Toast.makeText(getActivity(), "已复制到剪贴板", Toast.LENGTH_SHORT).show();
		} else if (v == mTvPluginPath) {
			String plainText = mTvPluginPath.getText().toString().trim();
			ShumiSdkClipboardUtil.copyToClipboard(getActivity(), plainText);
			Toast.makeText(getActivity(), "已复制到剪贴板", Toast.LENGTH_SHORT).show();
		}else if(v == mTvConsumer){
			// getActivity().openContextMenu(mBtnSwitchConsumer);
		}
		
		udpateContentView();
	}
	
	private void udpateContentView(){
		
		mBtnSwitchEnv.setText(ShumiSdkEnvSettings.getEnvUrl());
		mBtnSwitchUpdate.setText(ShumiSdkEnvSettings.getUpdateUrl());
		
		//mTvConsumer.setText(ShumiSdkConsumerConfig.getConsumerName(getActivity()));
		mBtnSwitchConsumer.setText(ShumiSdkConsumerConfig.getConsumerName(getActivity()));
		
		String url = ShumiSdkEnv.getUpdateURL();
		int index = url.indexOf("?id");
		if(index > 0){
			url = url.substring(0, index);
		}
		mTvUpdateUrl.setText(url);
		
		testUrl(url);
		
		String baseDir = ShumiSdkEnv.getBaseDir();
		mTvPluginPath.setText(ShumiSdkEnv.getBaseDir());
		if(new File(baseDir).exists()){
			mTvPluginPath.setTextColor(Color.BLACK);
		}else{
			mTvPluginPath.setTextColor(Color.GRAY);
		}
	}
	
	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(msg.arg1 == 200){
				mTvUpdateUrl.setTextColor(Color.BLACK);
			}else{
				mTvUpdateUrl.setTextColor(Color.GRAY);
			}
		};
	};
	
	private void testUrl(final String urlStr) {

		mExecutorService.execute(new Runnable() {

			@Override
			public void run() {
				try {
					URL url = new URL(urlStr);
					HttpURLConnection conn = (HttpURLConnection) url
							.openConnection();
					int code = conn.getResponseCode();
					Message msg = handler.obtainMessage();
					msg.arg1 = code;
					handler.sendMessage(msg);
				} catch (Exception e) {
				}

			}
		});
	}

}
