package com.shumi.plugin_sdk.demo.bean;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataManager;

public class Authorization extends BaseFunction {

	public Authorization(String name, boolean isUserLevel) {
		super(name, isUserLevel);
	}

	@Override
	public void execute(Context context) {
		final Context ctx = context;
		if (ShumiSdkDemoDataManager.getInstance().isAuthorised()) {

			new AlertDialog.Builder(ctx).setTitle("授权提示")
					.setMessage("将清除当前授权信息，继续授权？")
					.setPositiveButton("确定", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							ShumiSdkDemoDataManager.getInstance().reset();
							ShumiSdkTradingHelper.doAuthentication(ctx);
						}
					}).setNegativeButton("取消", null).create().show();

		} else {
			ShumiSdkDemoDataManager.getInstance().reset();
			ShumiSdkTradingHelper.doAuthentication(ctx);
		}

	}

}
