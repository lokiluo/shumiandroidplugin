package com.shumi.plugin_sdk.demo.bean;

import android.content.Context;

public interface IFunction {

	public boolean isUserLevel();

	public String getName();

	public void execute(Context context);
}
