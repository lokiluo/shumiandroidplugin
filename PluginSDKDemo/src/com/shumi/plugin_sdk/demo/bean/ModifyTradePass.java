package com.shumi.plugin_sdk.demo.bean;

import android.content.Context;

import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;

public class ModifyTradePass extends BaseFunction {

	public ModifyTradePass(String name, boolean isUserLevel) {
		super(name, isUserLevel);
	}

	@Override
	public void execute(Context context) {
		ShumiSdkTradingHelper.doChangeTradePassword(context);
	}

}
