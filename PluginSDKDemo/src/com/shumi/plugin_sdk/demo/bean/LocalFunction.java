package com.shumi.plugin_sdk.demo.bean;

import com.shumi.plugin_sdk.demo.activity.FuncActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

public class LocalFunction extends BaseFunction {

	private Class<Fragment> fragmentType;

	@SuppressWarnings("unchecked")
	public <T extends Fragment> LocalFunction(String name, boolean isUserLevel,
			Class<T> fragmentType) {
		super(name, isUserLevel);
		this.fragmentType = (Class<Fragment>) fragmentType;
	}

	public Fragment getFragment() {
		Fragment fragment = null;
		if(fragmentType != null){
			try {
				fragment = fragmentType.newInstance();
			} catch (Exception e) {
			}
		}
		return fragment;
	}

	@Override
	public void execute(Context context) {
		Intent intent = new Intent(context, FuncActivity.class);
		intent.putExtra(FuncActivity.FUNC, getName());
		context.startActivity(intent);
	}

}
