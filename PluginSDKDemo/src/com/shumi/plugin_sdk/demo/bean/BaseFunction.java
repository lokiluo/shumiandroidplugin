package com.shumi.plugin_sdk.demo.bean;

public abstract class BaseFunction implements IFunction {

	private String name;
	private boolean isUserLevel;

	public BaseFunction(String name, boolean isUserLevel) {
		this.name = name;
		this.isUserLevel = isUserLevel;
	}

	@Override
	public boolean isUserLevel() {
		return isUserLevel;
	}

	@Override
	public String getName() {
		return name;
	}

}
