package com.shumi.plugin_sdk.demo.bean;

import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;

import android.content.Context;

public class AddBankCard extends BaseFunction{

	public AddBankCard(String name, boolean isUserLevel) {
		super(name, isUserLevel);
	}

	@Override
	public void execute(Context context) {
		ShumiSdkTradingHelper.doAddBankCard(context);
	}

}
