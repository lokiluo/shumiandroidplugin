package com.shumi.plugin_sdk.demo;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.shumi.sdk.env.ShumiSdkData;

public class ShumiSdkConsumerConfig {

	protected static final List<Consumer> list = new ArrayList<Consumer>();

	public static class Consumer {

		public String Key;
		public String Name;
		public String SK;

		public Consumer(String key, String name, String sk) {
			this.Key = key;
			this.Name = name;
			this.SK = sk;
		}
	}

	private static Consumer getByKey(String key) {
		Consumer consumer = null;
		for (int i = 0; i < list.size(); i++) {
			consumer = list.get(i);
			if (key.equalsIgnoreCase(consumer.Key)) {
				break;
			}
		}
		return consumer;
	}

	private static Consumer getByName(String name) {
		Consumer consumer = null;
		for (int i = 0; i < list.size(); i++) {
			consumer = list.get(i);
			if (name.equalsIgnoreCase(consumer.Name)) {
				break;
			}
		}
		return consumer;
	}

	public static String getConsumerName(Context context) {
		String key = ShumiSdkData.getInstance(context).getConsumer();
		Consumer consumer = getByKey(key);
		return consumer == null ? key : consumer.Name;
	}

	public static List<Consumer> getConsumers() {
		return list;
	}

	public static void switchConsumer(Context context, String name) {
		ShumiSdkData.getInstance(context).switchConsumer(getByName(name).SK);
	}

}
