package com.shumi.plugin_sdk.demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.shumi.sdk.env.ShumiSdkEnv;

public class ShumiSdkEnvSettings {

	private static boolean IS_ENV_CHANGED = false;
	private static final String UPDATEURL = "updateUrl";
	private static final String PLUGINENV = "pluginEnv";

	public static void init(Context context) {

		SharedPreferences preferences = context.getSharedPreferences(
				"ShumiSdkEnvSetting", Context.MODE_PRIVATE);
		int updateUrl = preferences.getInt(UPDATEURL, 0);
		int pluginEnv = preferences.getInt(PLUGINENV, 0);

		if (updateUrl == 1) {
			ShumiSdkEnv.switchToBackupUpdate(true);
		} else {
			ShumiSdkEnv.switchToBackupUpdate(false);
		}

		if (pluginEnv == 2) {
			ShumiSdkEnv.switchToTesting();
		} else if (pluginEnv == 1) {
			ShumiSdkEnv.switchToSandbox();
		} else {
			ShumiSdkEnv.switchToOnline();
		}

	}

	public static boolean isEnvChanged() {
		boolean result = IS_ENV_CHANGED;
		if (IS_ENV_CHANGED) {
			IS_ENV_CHANGED = false;
		}
		return result;
	}

	public static void saveDisc(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(
				"ShumiSdkEnvSetting", Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		if (ShumiSdkEnv.isBackupUpdate()) {
			editor.putInt(UPDATEURL, 1);
		} else {
			editor.putInt(UPDATEURL, 0);
		}
		if (ShumiSdkEnv.isSandbox()) {
			editor.putInt(PLUGINENV, 2);
		} else if (ShumiSdkEnv.isTesting()) {
			editor.putInt(PLUGINENV, 1);
		} else {
			editor.putInt(PLUGINENV, 0);
		}
		editor.commit();
	}

	public static void switchEnv() {
		IS_ENV_CHANGED = true;
		if (ShumiSdkEnv.isOnline()) {
			ShumiSdkEnv.switchToSandbox();
		} else if (ShumiSdkEnv.isSandbox()) {
			ShumiSdkEnv.switchToTesting();
		} else if (ShumiSdkEnv.isTesting()) {
			ShumiSdkEnv.switchToOnline();
		}

	}

	public static String getEnvUrl() {
		String result = "生产环境";
		if (ShumiSdkEnv.isOnline()) {
			result = "生产环境";
		} else if (ShumiSdkEnv.isSandbox()) {
			result = "沙箱环境";
		} else if (ShumiSdkEnv.isTesting()) {
			result = "测试环境";
		}
		return result;
	}

	public static void switchUpdateUrl() {
		if (ShumiSdkEnv.isBackupUpdate()) {
			ShumiSdkEnv.switchToBackupUpdate(false);
		} else {
			ShumiSdkEnv.switchToBackupUpdate(true);
		}
	}

	public static String getUpdateUrl() {
		String result = "正式地址";
		if (ShumiSdkEnv.isBackupUpdate()) {
			result = "测试地址";
		} else {
			result = "正式地址";
		}
		return result;
	}

	public static String getUpdateStr() {
		return !ShumiSdkEnv.isBackupUpdate() ? "正式址址 √" : "正式地址";
	}

	public static String getTestUpdateStr() {
		return ShumiSdkEnv.isBackupUpdate() ? "测式址址 √" : "测式地址";
	}

	public static String getOnlineEnvStr() {
		return ShumiSdkEnv.isOnline() ? "生产环境 √" : "生产环境";
	}

	public static String getSandBoxEnvStr() {
		return ShumiSdkEnv.isSandbox() ? "沙箱环境 √" : "沙箱环境";
	}

	public static String getTestEvnStr() {
		return ShumiSdkEnv.isTesting() ? "测试环境 √" : "测试环境";
	}

	public static String getStr(int index) {
		if (index == 0) {
			return getUpdateStr();
		} else if (index == 1) {
			return getTestUpdateStr();
		} else if (index == 2) {
			return getOnlineEnvStr();
		} else if (index == 3) {
			return getSandBoxEnvStr();
		} else if (index == 4) {
			return getTestEvnStr();
		} else {
			return "更多设置";
		}
	}

}
