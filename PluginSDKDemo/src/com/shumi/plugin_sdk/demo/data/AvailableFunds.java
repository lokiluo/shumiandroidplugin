package com.shumi.plugin_sdk.demo.data;

import java.util.ArrayList;
import java.util.List;

import com.shumi.sdk.ext.data.bean.ShumiSdkTradeAvailableFundsBean;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeAvailableFundsBean.Item;

public class AvailableFunds {

	private static List<Item> list = new ArrayList<ShumiSdkTradeAvailableFundsBean.Item>();

	public static List<Item> getItem(String key) {
		List<Item> result = new ArrayList<Item>();
		for (Item item : list) {

			if (item.FundName.contains(key)) {
				result.add(item);
				continue;
			}

			if (item.FundCode.contains(key)) {
				result.add(item);
				continue;
			}

		}
		return result;
	}

	public static List<Item> getItem(int type) {
		String key = String.valueOf(type);
		List<Item> result = new ArrayList<Item>();
		for (Item item : list) {
			if (item.FundType.equalsIgnoreCase(key)) {
				result.add(item);
			}
		}
		return result;
	}

	public static void setItem(List<Item> items) {
		list = items;
	}

	public static List<Item> getItem() {
		return list;
	}

}
