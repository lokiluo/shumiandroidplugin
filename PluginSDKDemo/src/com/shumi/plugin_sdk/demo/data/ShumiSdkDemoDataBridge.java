package com.shumi.plugin_sdk.demo.data;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.shumi.sdk.IShumiSdkDataBridgeEx;
import com.shumi.sdk.data.eventargs.ShumiSdkAddBankCardEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkAuthorizedEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkBuyFundEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkCancelOrderEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkChangeMobileEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkCreateOrderEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkEventArgs;
import com.shumi.sdk.data.eventargs.ShumiSdkRedeemFundEventArgs;
import com.shumi.sdk.data.param.ShumiSdkConsumerUserInfo;

/**
 * 数米2.0使用的数据桥接口 负责插件(WebBundle)和SDK之间的双向交互 直接实现此接口中的所有方法
 * 
 * @author John
 * 
 */
public class ShumiSdkDemoDataBridge implements IShumiSdkDataBridgeEx {
	private final static String LogTag = "ShumiSdkDemoDataBridge";
	private static ShumiSdkDemoDataBridge instance;

	public static ShumiSdkDemoDataBridge getInstance(Context context) {
		if (instance == null) {
			instance = new ShumiSdkDemoDataBridge();
		}
		instance.mContext = context;
		return instance;
	}

	private ShumiSdkDemoDataBridge() {
	}

	private Context mContext;
	private ShumiSdkDemoDataManager mManager = ShumiSdkDemoDataManager
			.getInstance();
	private Handler mHandler = new Handler();

	public void showToast(final String message) {
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void init(Context context) {
		this.mContext = context;
	}

	public void setToken(String v) {
		mManager.setAccessToken(v);
	}

	public void setTokenSecret(String v) {
		mManager.setAccessTokenSecret(v);
	}

	@Override
	public String getConsumerKey() {
		return mManager.getConsumerKey();
	}

	@Override
	public String getConsumerSecret() {
		return mManager.getConsumerSecret();
	}

	@Override
	public String getAccessToken() {
		return mManager.getAccessToken();
	}

	@Override
	public String getAccessTokenSecret() {
		return mManager.getAccessTokenSecret();
	}

	@Override
	public ShumiSdkConsumerUserInfo getConsumerUserInfo() {
		ShumiSdkConsumerUserInfo user = new ShumiSdkConsumerUserInfo();
		user.setRealName(mManager.getRealName());
		user.setIdNumber(mManager.getIdNumber());
		user.setEmailAddr(mManager.getEmailAddr());
		user.setPhoneNum(mManager.getPhoneNum());
		return user;
	}

	@Override
	public void onSdkEvtAuthorized(ShumiSdkAuthorizedEventArgs eventArgs) {
		mManager.setAccessToken(eventArgs.getAccessToken());
		mManager.setAccessTokenSecret(eventArgs.getAccessTokenSecret());
		mManager.setIdNumber(eventArgs.getIdNumber());
		mManager.setRealName(eventArgs.getRealName());
		//String str = eventArgs.isNewAccount() ? "新开户" : "已绑定数米用户"
		//		+ eventArgs.toString();
		//Toast.makeText(mContext, str, Toast.LENGTH_LONG).show();
		//showToast("已经绑定用户：" + mManager.getRealName());
	}

	@Override
	public void onSdkEvtBuySuccessed(ShumiSdkBuyFundEventArgs eventArgs) {
		Log.d(LogTag, "onSdkEvtBuySuccessed =>" + eventArgs.toString());
		showToast("申购成功");
	}

	@Override
	public void onSdkEvtRedeemSuccessed(ShumiSdkRedeemFundEventArgs eventArgs) {
		Log.d(LogTag, "onSdkEvtRedeemSuccessed =>" + eventArgs.toString());
		showToast("赎回成功");
	}

	@Override
	public void onSdkEvtCancelOrderSuccessed(
			ShumiSdkCancelOrderEventArgs eventArgs) {
		Log.d(LogTag, "onSdkEvtCancelOrderSuccessed =>" + eventArgs.toString());
		showToast("撤单成功");
	}

	@Override
	public void onSdkEvtAddBankCardSuccessed(
			ShumiSdkAddBankCardEventArgs eventArgs) {
		Log.d(LogTag, "onSdkEvtAddBankCardSuccessed =>" + eventArgs.toString());
		showToast("添加银行卡成功");
	}

	@Override
	public void onSdkEvtNotHandled(ShumiSdkEventArgs eventArgs) {
		Log.d(LogTag, "onSdkEvtNotHandled =>" + eventArgs.toString());
		showToast("未处理事件");
	}

	public void resetTokenAndSecret() {
		mManager.reset();
	}

	public boolean isAuthorization() {
		return !TextUtils.isEmpty(mManager.getAccessToken());
	}

	public String getRealName() {
		return mManager.getRealName();
	}

	@Override
	public void onSdkEvtRegularInvestFundSuccess(ShumiSdkEventArgs eventArgs) {
		
	}

	@Override
	public void onSdkEvtModifyRegularInvestFundSuccess(
			ShumiSdkEventArgs eventArgs) {
		
	}

	@Override
	public void onSdkEvtTransformFundSuccess(ShumiSdkEventArgs eventArgs) {
		
	}

	@Override
	public void onSdkEvtChangeMobileSuccessed(
			ShumiSdkChangeMobileEventArgs eventArgs) {
		
	}

	@Override
	public void onSdkEvtCreateOrderSuccessed(ShumiSdkCreateOrderEventArgs eventArgs) {
		showToast("订单编号："+eventArgs.getApplySerial());
	}
}
