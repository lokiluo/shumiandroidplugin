package com.shumi.plugin_sdk.demo.data;

import java.util.Map;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.fund123.sdk.delegator.Fund123OauthInterface;
import com.fund123.sdk.delegator.params.ConsumerUserInfo;
import com.shumi.sdk.data.eventargs.ShumiSdkAuthorizedEventArgs;

/**
 * 实现数米老版本的Fund123DemoDataBridge
 * 建议使用新版本ShumiSdkDemoDataBridge代替，
 * 这里只是显示如何在不修改数米交易1.0插件的情况下使用
 * @author John
 *
 */
@Deprecated
public class Fund123DemoDataBridge implements Fund123OauthInterface {
	private String LogTag;
	public static Fund123DemoDataBridge instance;
	// 用于存放信息的管理类
	private ShumiSdkDemoDataManager manager = ShumiSdkDemoDataManager.getInstance();
		
	public static Fund123DemoDataBridge getInstance(Context context) {
		if (instance == null) {
			instance = new Fund123DemoDataBridge(context);
		}
		if (instance.mContext != context.getApplicationContext()) {
			instance.mContext = context.getApplicationContext();
		}
		return instance;
	}

	// 上下问
	private Context mContext;

	private Handler handler = new Handler();
	
	private Fund123DemoDataBridge(Context context) {		
		LogTag = this.getClass().getName();
		mContext = context.getApplicationContext();
	}
	
	public void setContext(Context context) {
		this.mContext = context.getApplicationContext();
	}
	
	public void showToast(final String message) {
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	/**
	 * 返回给数米网第三方的授权KEY
	 */
	public String returnOauthConsumerKey() {
		return ShumiSdkDemoDataManager.getInstance().getConsumerKey();
	}

	@Override
	/**
	 * 返回给数米网第三方的授权secret
	 */
	public String returnOauthConsumerSecret() {
		return ShumiSdkDemoDataManager.getInstance().getConsumerSecret();
	}

	@Override
	/**
	 * 返回给数米网第三方用户的授权KEY
	 */
	public String returnOauthToken() {
		return ShumiSdkDemoDataManager.getInstance().getAccessToken();
	}

	@Override
	/**
	 * 返回给数米网第三方用户的授权secret
	 */
	public String returnOauthTokenSecret() {
		return ShumiSdkDemoDataManager.getInstance().getAccessTokenSecret();
	}

	@Override
	/**
	 * 用户授权之后，返回给第三方该用户的授权KEY和secret，第三方要注意保存该数值
	 * 等同于 IShumiSdkDataBridge.onSdkEvtAuthorized
	 */
	public void getAouthToken(Map<String, String> oauthTokenInfo) {
		Log.v(LogTag, "getAouthToken =>" + oauthTokenInfo.toString());
		// 数米交易插件2.0加入EvtArgs方法，实现接口Map，可以和map一样操作
		ShumiSdkAuthorizedEventArgs eventArgs = new ShumiSdkAuthorizedEventArgs(oauthTokenInfo);
		/***
		 *  获得从插件中返回的回调函数
		 *  从oauthTokenInfo或者eventArgs中获得"authorizedToken"，以下两种方法等效<br>
		 *  String oauthToken = oauthTokenInfo.get("authorizedToken");<br>
		 *  或者：String oauthToken = eventArgs.getAccessToken();<br>
		 *  这个demo使用2.0新增的方法ShumiSdkAuthorizedEventArgs获得
		 */
		manager.setAccessToken(eventArgs.getAccessToken());
		manager.setAccessTokenSecret(eventArgs.getAccessTokenSecret());
		manager.setRealName(eventArgs.getRealName());
		manager.setIdNumber(eventArgs.getIdNumber());
		showToast("已经绑定用户：" + manager.getRealName());
	}

	@Override
	public String returnUserPhoneNum() {
		return manager.getPhoneNum();
	}

	@Override
	public void userPurchaseFundSuccess(Map<String, String> fund123EventInfo) {
		Log.v(LogTag, "userPurchaseFundSuccess =>" + fund123EventInfo.toString());
		showToast("申购成功");
	}

	@Override
	public void userRedeemFundSuccess(Map<String, String> fund123EventInfo) {
		Log.v(LogTag, "userRedeemFundSuccess:" + fund123EventInfo.toString());
		showToast("赎回成功");
	}

	@Override
	public ConsumerUserInfo readConsumerUserInfo() {
		ConsumerUserInfo consumerUserInfo = new ConsumerUserInfo();
		consumerUserInfo.IdNumber = manager.getIdNumber();
		consumerUserInfo.RealName = manager.getRealName();
		consumerUserInfo.EmailAddr = manager.getEmailAddr();
		consumerUserInfo.PhoneNum = manager.getPhoneNum();
		return consumerUserInfo;
	}

	@Override
	public void userCancelOrderSuccess(Map<String, String> fund123EventInfo) {
		Log.v(LogTag, "userCancelOrderSuccess =>" + fund123EventInfo.toString());
		showToast("撤单成功");
	}

	@Override
	public void userAddBankCardSuccess(Map<String, String> fund123EventInfo) {
		Log.v(LogTag, "userAddBankCardSuccess =>" + fund123EventInfo.toString());
		showToast( "添加银行卡成功");
	}

	@Override
	public void onSdkEvent(final String eventTag, Map<String, String> eventInfo) {
		Log.v(LogTag, String.format("%s => %s", eventTag + eventInfo.toString()));
		showToast("SdkEvent: " + eventTag);
	}
}
