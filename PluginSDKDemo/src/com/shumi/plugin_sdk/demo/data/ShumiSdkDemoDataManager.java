package com.shumi.plugin_sdk.demo.data;

import java.util.Properties;

import android.text.TextUtils;

import com.shumi.plugin_sdk.demo.ShumiSdkApplication;

/**
 * 信息管理存放管理类<br>
 * DEMO中部分接口为了兼容老版本，使用Fund123OauthInterface做演示
 * 所以在这里保存Fund123OauthInterface和IShumiSdkDataBridge共同使用的数据
 * @author John
 *
 */
public final class ShumiSdkDemoDataManager {
	private static ShumiSdkDemoDataManager mInstance;
	// TODO 设置consumerKey
	// oauth consumer key
	private String mConsumerKey = "smb";
	
	// TODO 设置consumerSecret
	// oauth consumer secret
	private String mConsumerSecret = "smb888";
	// oauth 用户token
	private String mAccessToken;
	// oauth 用户tokensecret
	private String mAccessTokenSecret;
	// 真实姓名
	private String mRealName = "";
	// 身份证号
	private String mIdNumber = "";
	// 电话号码
	private String mPhoneNum = "";
	// 电子邮件
	private String mEmailAddr = "";
	// 数米用户名
	private String mShumiUserName;
	
	public String getShumiUserName() {
		return mShumiUserName;
	}

	public void setShumiUserName(String mShumiUserName) {
		this.mShumiUserName = mShumiUserName;
	}

	public boolean isAuthorised() {
		return !TextUtils.isEmpty(mAccessToken);
	}
	
	public String getRealName() {
		return mRealName;
	}

	public void setRealName(String realName) {
		this.mRealName = realName;
	}

	public String getIdNumber() {
		return mIdNumber;
	}

	public void setIdNumber(String idNumber) {
		this.mIdNumber = idNumber;
	}

	public String getPhoneNum() {
		return mPhoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.mPhoneNum = phoneNum;
	}

	public String getEmailAddr() {
		return mEmailAddr;
	}

	public void setEmailAddr(String emailAddr) {
		this.mEmailAddr = emailAddr;
	}

	public static ShumiSdkDemoDataManager getInstance() {
		if (mInstance == null) {
			mInstance = new ShumiSdkDemoDataManager();
		}
		return mInstance;
	}
	
	private void initProperties() {
		try {
			Properties p = new Properties();
			p.load(ShumiSdkApplication.getInstance().getAssets()
					.open("properties/user.properties"));
			mConsumerKey = p.getProperty("auth.consumerkey", "");
			mConsumerSecret = p.getProperty("auth.consumerSecret", "");
		} catch (Exception e) {
		}
	}
	
	private ShumiSdkDemoDataManager() {
		initProperties();
	}

	
	public String getConsumerKey() {
		return mConsumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.mConsumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return mConsumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.mConsumerSecret = consumerSecret;
	}

	public String getAccessToken() {
		return mAccessToken;
	}

	public void setAccessToken(String accessToken) {
		this.mAccessToken = accessToken;
	}

	public String getAccessTokenSecret() {
		return mAccessTokenSecret;
	}

	public void setAccessTokenSecret(String accessTokenSecret) {
		this.mAccessTokenSecret = accessTokenSecret;
	}

	public void reset() {
		mAccessToken = "";
		mAccessTokenSecret = "";
		mIdNumber = "";
		mRealName = "";
		mShumiUserName = "";
		mPhoneNum = "";
		mEmailAddr = "";
	}
	
	/**
	 * 随机产生一个手机号码和EMAIL地址
	 * 用来测试
	 */
	public void randomPhoneAndEmail() {
		
	}
}
