package com.shumi.plugin_sdk.demo;

import android.app.Application;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataManager;
import com.shumi.sdk.ShumiSdkInitializer;
import com.shumi.sdk.logging.ShumiSdkLogger;

public class ShumiSdkApplication extends Application {

	public static int ScreenWidth;
	public static int ScreenHeihgt;

	private static ShumiSdkApplication instance;

	@Override
	public void onCreate() {
		super.onCreate();
		
		instance = this;
		ShumiSdkInitializer.init(this);
		ShumiSdkEnvSettings.init(this);
		initScreenParam();
		initDataManager();
		ShumiSdkInitConfig.init();
		
		ShumiSdkLogger.getInstance().deleteObservers();
		ShumiSdkLogger.getInstance().addObserver(
				ShumiSdkDemoLogHandler.Instance);
	}
	
	public static void initDataManager(){
		ShumiSdkDemoDataManager.getInstance().setConsumerKey(ShumiSdkInitConfig.CONSUMER_KEY);
		ShumiSdkDemoDataManager.getInstance().setConsumerSecret(ShumiSdkInitConfig.CONSUMER_SECRET);
		ShumiSdkDemoDataManager.getInstance().setAccessToken(ShumiSdkInitConfig.ACCESS_TOKEN);
		ShumiSdkDemoDataManager.getInstance().setAccessTokenSecret(ShumiSdkInitConfig.TOKEN_SECRET);
	}

	protected void initScreenParam() {
		WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics outMetrics = new DisplayMetrics();
		manager.getDefaultDisplay().getMetrics(outMetrics);
		ScreenWidth = outMetrics.widthPixels;
		ScreenHeihgt = outMetrics.heightPixels;
	}

	public static ShumiSdkApplication getInstance() {
		return instance;
	}

}
