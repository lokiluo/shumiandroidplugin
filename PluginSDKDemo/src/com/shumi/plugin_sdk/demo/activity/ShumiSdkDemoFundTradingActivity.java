package com.shumi.plugin_sdk.demo.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.plugin_sdk.demo.fragment.LoadingDialogFragment;
import com.shumi.sdk.fragment.ShumiSdkFundTradingFragment;

/**
 * 交易主Activity
 * @author John
 *
 */
public class ShumiSdkDemoFundTradingActivity extends ActionBarActivity {
	/**
	 * 交易Fragment
	 * 如果使用内部类(Inner Class)，请保证此类为public static，并且含无参构造
	 * 否则被GC回收后再切入前台会发生InstantiationException
	 * @author John
	 *
	 */
	public static class FundTradingFragment extends ShumiSdkFundTradingFragment {
		private LoadingDialogFragment dialogFragment;
		private Handler mHander = new Handler();
		
		/**
		 * 执行退出Sdk的操作
		 * 可以finish所在Activity，或者使用FragmentManager将其replace掉
		 */
		@Override
		public void doQuitSdk() {
			getActivity().finish();
		}

		/**
		 * 自定义Loading界面
		 * 第三方可以换入自己的Logo
		 */
		@Override
		protected View getLoadingView() {
			return getActivity().getLayoutInflater().inflate(R.layout.shumi_sdk_bg, null);
		}
		
		/** 自定义下载开始、下载中、下载结束时的提示信息 **/
		/**
		 * 下载开始
		 */
		@Override
		public void onUpdateStart(Object sender) {
			FragmentTransaction ft = getFragmentManager()
					.beginTransaction();
			Fragment prev = getFragmentManager()
					.findFragmentByTag("dialog");
			if (dialogFragment != null) {
				ft.remove(prev);
			}
			ft.addToBackStack(null);
			dialogFragment = new LoadingDialogFragment();
			dialogFragment.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					try {
						ShumiSdkFundTradingFragment f = (ShumiSdkFundTradingFragment)
								getFragmentManager().findFragmentById(R.id.content);
						f.cancelDownloading();
						getActivity().finish();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			dialogFragment.show(ft, "dialog");
		}
		
		/**
		 * 下载完成
		 */
		@Override
		public void onUpdateFinish(Object sender) {
			if (dialogFragment != null) {
				dialogFragment.setProgress(100);
				mHander.postDelayed(new Runnable() {

					@Override
					public void run() {
						try {
							if (dialogFragment != null) {
								dialogFragment.dismiss();
							}
						} catch (Exception e) {

						}
					}
				}, 800);
			}
		}
		
		/**
		 * 下载中
		 */
		@Override
		public void onUpdateProgress(int bytesWritten, int totalSize,
				Object sender) {
			if (dialogFragment != null) {
				dialogFragment.setProgress(bytesWritten * 100 / totalSize);
			}
		}
	}

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		getSupportActionBar().hide();
		setContentView(R.layout.shumi_sdk_component_fund_trading);
		// 设置Fragment
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content, createShumiSdkFundTradingFragment())
				.commit();
	}
	
	/**
	 * 创建数米SdkTradingFragment<br>
	 * 需要注意Fragment使用方式，否则detach之后，getActivity()之后返回null
	 * 
	 * @return
	 */
	protected ShumiSdkFundTradingFragment createShumiSdkFundTradingFragment() {
		// TODO 定义一个合适的ShumiSdkFundTradingFragment，重写方法
		ShumiSdkFundTradingFragment tradingFragment = new FundTradingFragment();
		
		/**
		 *  TODO
		 *  这里可以传入实现IShumiSdkDataBridge的接口(推荐)
		 *  或者传入实现IFund123OauthInterface的接口
		 */
		tradingFragment.setDataBridge(ShumiSdkDemoDataBridge.getInstance(this));
//		tradingFragment.setDataBridge(Fund123DemoDataBridge.getInstance(this));
		
		// TODO 加入页面切换的动画
		Animation animPageEnter = AnimationUtils.loadAnimation(this,
				R.anim.slide_right_in);
		Animation animPageLeave = AnimationUtils.loadAnimation(this,
				R.anim.slide_right_out);
		tradingFragment.setPageAnimation(animPageEnter, animPageLeave);
		
		// 写入参数
		tradingFragment.setArguments(getIntent().getExtras());
		return tradingFragment;
	}

	/**
	 * 重写按下返回键时间的操作
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Fragment fragment = getSupportFragmentManager().findFragmentById(
				R.id.content);
		if (fragment instanceof ShumiSdkFundTradingFragment) {
			/**
			 * ShumiSdkFundTradingFragment 中实现了一个KeyEvent
			 */
			ShumiSdkFundTradingFragment fundTradingFragment = (ShumiSdkFundTradingFragment) fragment;
			// 调用ShumiSdkFundTradingFragment的onKeyDown方法，
			// 如果返回TRUE则不处理，返回FALSE继续处理
			if (fundTradingFragment.onKeyDown(keyCode, event)) {
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
