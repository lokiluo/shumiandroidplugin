package com.shumi.plugin_sdk.demo.activity;

import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkTradingHelper;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.plugin_sdk.demo.util.ShumiSdkDemoValueFormator;
import com.shumi.sdk.data.bean.ShumiSdkCodeMessageBean;
import com.shumi.sdk.data.param.trade.general.ShumiSdkCancelOrderParam;
import com.shumi.sdk.data.service.openapi.IShumiSdkOpenApiDataServiceHandler;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeApplyRecordsBean;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeFundSharesBean;
import com.shumi.sdk.ext.data.service.ShumiSdkApplyRecordsByFundCodeDataService;
import com.shumi.sdk.ext.data.service.ShumiSdkApplyRecordsDataService;
import com.shumi.sdk.ext.util.ShumiSdkFundTradingDictionary;
import com.shumi.sdk.ext.util.ShumiSdkFundTradingDictionary.Dictionary;

/**
 * 交易申请记录
 * @author John
 *
 */
public class ShumiSdkDemoFundApplyRecordsActivity extends ActionBarActivity
		implements OnScrollListener, IShumiSdkOpenApiDataServiceHandler {
	public static final String TAG_EXTRA_FUNDSHARE = "EXTRA.FundShareData";
	private static final String LogTag = ShumiSdkDemoFundApplyRecordsActivity.class
			.getName();

	// private String mStrBeginDate;
	// private String mStrEndDate;
	private ListView mLvApplyRecords;
	private ApplyRecordsDataAdapter mAdapter;
	// private ProgressDialog mProgressDialog;
	private AtomicBoolean mHasMoreData = new AtomicBoolean(false);

	// 交易记录请求
	private ShumiSdkApplyRecordsByFundCodeDataService mDataService;
	// 交易记录请求参数
	private ShumiSdkApplyRecordsByFundCodeDataService.Param mParam;
	// 默认分页
	private static final int defaultPageSize = 20;
	// 当前显示的持仓基金
	private ShumiSdkTradeFundSharesBean.Item mFundShareBeanItem = null;
	// 字典
	public ShumiSdkFundTradingDictionary mDictionary;

	/**
	 * 初始化请求
	 */
	private void initDataService() {
		mDataService = new ShumiSdkApplyRecordsByFundCodeDataService(this,
				ShumiSdkDemoDataBridge.getInstance(this));
		mParam = new ShumiSdkApplyRecordsByFundCodeDataService.Param();
		mDataService.setDataServiceCallback(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		setContentView(R.layout.shumi_sdk_component_apply_records_single_fund);
		if (getIntent().hasExtra(TAG_EXTRA_FUNDSHARE)) {
			mFundShareBeanItem = (ShumiSdkTradeFundSharesBean.Item) getIntent()
					.getExtras().get(TAG_EXTRA_FUNDSHARE);
		}
		getSupportActionBar().setTitle(R.string.apply_records_detail);
		getSupportActionBar().setSubtitle(
				String.format("%s %s", mFundShareBeanItem.FundCode,
						mFundShareBeanItem.FundName));

		mLvApplyRecords = (ListView) findViewById(R.id.listViewApplyRecords);
		mAdapter = new ApplyRecordsDataAdapter(this);
		mLvApplyRecords.setAdapter(mAdapter);
		mLvApplyRecords.setOnScrollListener(this);
		mDictionary = ShumiSdkFundTradingDictionary.getInstance(this);
		initDataService();
//		initQueryDatePeriod();
		doRequest();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.fundshares, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else if (item.getItemId() == R.id.refresh) {
			doRequest();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

//	private void initQueryDatePeriod() {
//		// 获取一个月的记录
//		Calendar calendar = Calendar.getInstance();
//		mStrEndDate = String.format("%04d-%02d-%02d",
//				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
//				calendar.get(Calendar.DAY_OF_MONTH));
//		calendar.add(Calendar.MONTH, -2);
//		mStrBeginDate = String.format("%04d-%02d-%02d",
//				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
//				calendar.get(Calendar.DAY_OF_MONTH));
//	}

	// 请求数据
	private void doRequest() {
		setSupportProgressBarIndeterminateVisibility(true);
		mLvApplyRecords.setVisibility(View.GONE);
		mHasMoreData.set(false);
		// 请求数据
		mDataService.cancel();
		mParam.PageIndex = 1;
		mParam.FundCode = mFundShareBeanItem.FundCode;
		mParam.PageSize = defaultPageSize;
		// 可以使用get或者post
		mDataService.post(mParam);
	}

	private void doRequestMore() {
		setSupportProgressBarIndeterminateVisibility(true);
		mHasMoreData.set(false);
		// 请求数据
		mDataService.cancel();
		mParam.PageIndex += 1;
		mParam.FundCode = mFundShareBeanItem.FundCode;
		// 其他参数不变，可以不填写
		// 这次使用get方式
		mDataService.get(mParam);
	}

	@Override
	protected void onPause() {
		// 取消请求
		mDataService.cancel();
		setSupportProgressBarIndeterminateVisibility(false);
		super.onPause();
	}

	@Override
	public void onGetData(Object dataObject, Object sender) {
		if (mDataService == sender) {
			try {
				ShumiSdkTradeApplyRecordsBean bean = (ShumiSdkTradeApplyRecordsBean) dataObject;
				// 涉及到分页，如果当前是第一页，则清空表
				if (mParam.PageIndex == 1) {
					mAdapter.clear();
				}
				for (ShumiSdkTradeApplyRecordsBean.Item item : bean.Items) {
					mAdapter.add(item);
				}

				// TODO 请自行做好分页处理
				// 此DEMO的处理是快拉至底部请求数据
				// 当前页索引 * 每页记录数 小于 总记录数
				if (mParam.PageIndex * mParam.PageSize < bean.Total) {
					// TODO 仍有数据未加载完 此时只需要将mParam中的页数+1重新请求即可
					mHasMoreData.set(true);
				} else {
					mHasMoreData.set(false);
				}

			} catch (Exception e) {
				Log.e(LogTag, e.toString());
			} finally {
				mAdapter.notifyDataSetChanged();
				setSupportProgressBarIndeterminateVisibility(false);
				mLvApplyRecords.setVisibility(View.VISIBLE);
			}
		}

	}

	@Override
	public void onGetError(int code, String message, Throwable err,
			Object sender) {
		if (mDataService == sender) {
			try {
				/**
				 * TODO 判断是否是服务器维护,服务端维护时返回的错误代码code:500或502
				 * 并且ShumiSdkCodeMessageBean中，
				 * 维护时返回ShumiSdkCodeMessageBean.Code:99999，
				 * 维护信息返回ShumiSdkCodeMessageBean.Message
				 */
				ShumiSdkCodeMessageBean bean = ShumiSdkApplyRecordsDataService
						.getCodeMessage(message);
				if (!TextUtils.isEmpty(bean.getMessage())) {
					Toast.makeText(this, bean.getMessage(), Toast.LENGTH_LONG)
							.show();
				}

			} catch (Exception e) {
				setSupportProgressBarIndeterminateVisibility(false);
				mLvApplyRecords.setVisibility(View.VISIBLE);
			}
		}
	}

	public class ApplyRecordsDataAdapter extends
			ArrayAdapter<ShumiSdkTradeApplyRecordsBean.Item> {
		class ViewHolder {
			TextView mTvFundCodeAndName;
			TextView mTvPayResult;
			TextView mTvBankAcco;
			TextView mTvApplyTime;
			TextView mTvBusinessType;
			TextView mTvStatus;
			TextView mTvSumAndShare;
			Button mBtnCancelOrder;
		}

		public ApplyRecordsDataAdapter(Context context) {
			super(context, 0);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder vh = null;
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.shumi_sdk_list_item_apply_records, null);
				vh = new ViewHolder();
				vh.mTvFundCodeAndName = (TextView) convertView
						.findViewById(R.id.textViewFundCodeAndName);
				vh.mTvBankAcco = (TextView) convertView
						.findViewById(R.id.textViewBankAcco);
				vh.mTvPayResult = (TextView) convertView
						.findViewById(R.id.textViewPayResult);
				vh.mTvApplyTime = (TextView) convertView
						.findViewById(R.id.textViewApplyTime);
				vh.mTvBusinessType = (TextView) convertView
						.findViewById(R.id.textViewBusinessType);
				vh.mTvStatus = (TextView) convertView
						.findViewById(R.id.textViewStatus);
				vh.mTvSumAndShare = (TextView) convertView
						.findViewById(R.id.textViewApplySumAndShare);
				vh.mBtnCancelOrder = (Button) convertView
						.findViewById(R.id.buttonCancelOrder);
				vh.mBtnCancelOrder.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ShumiSdkTradeApplyRecordsBean.Item item = (ShumiSdkTradeApplyRecordsBean.Item) v
								.getTag();
						ShumiSdkCancelOrderParam param = new ShumiSdkCancelOrderParam();
						param.setParam(item.ApplySerial);
						ShumiSdkTradingHelper.doCancelOrder(
								ShumiSdkDemoFundApplyRecordsActivity.this,
								param);
					}
				});
			} else {
				vh = (ViewHolder) convertView.getTag();
			}
			ShumiSdkTradeApplyRecordsBean.Item item = getItem(position);
			vh.mTvFundCodeAndName.setText(String.format("%s %s", item.FundCode,
					item.FundName));
			String payResult = mDictionary.lookup(Dictionary.PayResult,
					item.PayResult);
			vh.mTvPayResult.setText(payResult);
			vh.mTvPayResult
					.setVisibility(TextUtils.isEmpty(payResult) ? View.GONE
							: View.VISIBLE);
			// 请届时处理好银行卡号的显示(显示后四位)
			vh.mTvBankAcco.setText(String.format("%s %s", item.BankName,
					item.BankAccount));
			vh.mTvApplyTime.setText(ShumiSdkDemoValueFormator
					.reformatDate(item.ApplyDateTime));
			vh.mTvBusinessType.setText(mDictionary.lookup(Dictionary.BusinFlag,
					item.BusinessType));
			vh.mTvStatus.setText(mDictionary.lookup(Dictionary.ConfirmState,
					item.Status));
			vh.mTvSumAndShare.setText(String.format("申请金额:%.2f 申请份额:%.2f",
					item.Amount, item.Shares));
			vh.mBtnCancelOrder.setVisibility(item.CanCancel ? View.VISIBLE
					: View.GONE);
			vh.mBtnCancelOrder.setTag(item);
			convertView.setTag(vh);
			return convertView;
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		int visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
		// 加载更多数据,快移动到底部(离底部2行以内)自动加载
		if (visibleLastIndex >= totalItemCount - 2 && mHasMoreData.get()) {
			doRequestMore();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}
}
