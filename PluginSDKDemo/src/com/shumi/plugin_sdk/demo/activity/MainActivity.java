package com.shumi.plugin_sdk.demo.activity;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.GridLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.myjson.Gson;
import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.ShumiSdkApplication;
import com.shumi.plugin_sdk.demo.ShumiSdkEnvSettings;
import com.shumi.plugin_sdk.demo.ShumiSdkFunctionManager;
import com.shumi.plugin_sdk.demo.bean.IFunction;
import com.shumi.plugin_sdk.demo.bean.DemoConfig;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataBridge;
import com.shumi.plugin_sdk.demo.data.ShumiSdkDemoDataManager;
import com.shumi.plugin_sdk.demo.util.UM;
import com.shumi.sdk.data.bean.ShumiSdkCodeMessageBean;
import com.shumi.sdk.data.service.openapi.IShumiSdkOpenApiDataServiceHandler;
import com.shumi.sdk.env.ShumiSdkData;
import com.shumi.sdk.env.ShumiSdkEnv;
import com.shumi.sdk.ext.data.bean.ShumiSdkTradeAccountBean;
import com.shumi.sdk.ext.data.service.ShumiSdkApplyRecordsDataService;
import com.shumi.sdk.ext.data.service.ShumiSdkGetAccountDataService;

public class MainActivity extends ActionBarActivity implements
		IShumiSdkOpenApiDataServiceHandler, OnClickListener {

	private long mlastTime = 0;
	private Menu mMenu;
	private GridLayout mContainer;
	private ShumiSdkGetAccountDataService mDataService;
	private AtomicBoolean isTokenValid = new AtomicBoolean(true);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		ShumiSdkApplication.initDataManager();
		setContentView(R.layout.activity_main);
		mContainer = (GridLayout) findViewById(R.id.container);
		getAccountInfo();
		Toast.makeText(this,
				"SDK版本：" + ShumiSdkData.getInstance(this).getSdkVersion(),
				Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateActionBar();
		updateFunctions();
		updateMenu();
		if (ShumiSdkEnvSettings.isEnvChanged()) {
			getAccountInfo();
		}
	}

	protected void updateFunctions() {
		mContainer.removeAllViews();
		List<IFunction> list = ShumiSdkFunctionManager.get();
		ShumiSdkDemoDataBridge data = ShumiSdkDemoDataBridge.getInstance(this);
		int size = ShumiSdkFunctionManager.size();
		int columnCount = 3;
		if (size > 4) {
			columnCount = 3;
		} else {
			columnCount = 3;
		}
		mContainer.setColumnCount(columnCount);
		int screenWidth = ShumiSdkApplication.ScreenWidth;
		int columWidth = (screenWidth - 120) / columnCount;

		for (IFunction func : list) {
			if (func.isUserLevel() && !data.isAuthorization()) {
				continue;
			}

			Button button = new Button(this);
			button.setText(func.getName());
			button.setTag(func);
			button.setOnClickListener(this);
			if (func == list.get(0)) {
				button.setOnLongClickListener(mOnLongClickListener);
			}
			ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(columWidth,
					columWidth);
			GridLayout.LayoutParams glp = new GridLayout.LayoutParams(lp);
			glp.setMargins(15, 15, 15, 15);
			mContainer.addView(button, glp);
		}
	}

	private Handler handler = new Handler();

	private OnLongClickListener mOnLongClickListener = new OnLongClickListener() {

		@Override
		public boolean onLongClick(View v) {
			Thread t = new Thread(new Runnable() {

				@Override
				public void run() {
					final String rs = UM.getUC();
					handler.post(new Runnable() {

						@Override
						public void run() {
							if (rs != null && rs.length() > 0) {
								DemoConfig dc = new Gson().fromJson(rs,
										DemoConfig.class);
								if (dc != null) {
									ShumiSdkDemoDataManager data = ShumiSdkDemoDataManager
											.getInstance();
									data.setConsumerKey(dc.CK);
									data.setConsumerSecret(dc.CS);
									data.setAccessToken(dc.TK);
									data.setAccessTokenSecret(dc.TS);
									getAccountInfo();
								}
							}
						}
					});
				}
			});
			t.start();
			return true;
		}
	};

	@Override
	public void onClick(View v) {
		IFunction func = (IFunction) v.getTag();
		if (func != null) {
			func.execute(this);
		}
	}

	protected void updateActionBar() {
		ActionBar actionBar = getSupportActionBar();
		ShumiSdkDemoDataBridge data = ShumiSdkDemoDataBridge.getInstance(this);

		if (data.isAuthorization()) {
			if (isTokenValid.get()) {
				actionBar.setTitle("已授权");
			} else {
				actionBar.setTitle("Token无效");
			}
		} else {
			actionBar.setTitle("未授权");
		}

		actionBar.setSubtitle(ShumiSdkEnvSettings.getEnvUrl());

		String name = data.getRealName();
		if (name != null && name.length() > 0) {
			actionBar.setTitle(name);
		}
	}

	protected void getAccountInfo() {
		ShumiSdkDemoDataBridge data = ShumiSdkDemoDataBridge.getInstance(this);
		if (ShumiSdkDemoDataManager.getInstance().isAuthorised()) {
			mDataService = new ShumiSdkGetAccountDataService(this, data);
			mDataService.setDataServiceCallback(this);
			setSupportProgressBarIndeterminateVisibility(true);
			getSupportActionBar().setTitle("loading...");
			mDataService.cancel();
			ShumiSdkDemoDataManager.getInstance().setRealName("");
			mDataService.get(null);
		} else {
			updateActionBar();
		}
	}

	@Override
	public void onGetData(Object dataObject, Object sender) {
		setSupportProgressBarIndeterminateVisibility(false);
		if (sender == mDataService) {
			ShumiSdkTradeAccountBean bean = (ShumiSdkTradeAccountBean) dataObject;
			ShumiSdkDemoDataManager dataManager = ShumiSdkDemoDataManager
					.getInstance();
			dataManager.setEmailAddr(bean.Email);
			dataManager.setIdNumber(bean.CertificateNumber);
			dataManager.setPhoneNum(bean.Mobile);
			dataManager.setRealName(bean.RealName);
			dataManager.setShumiUserName(bean.UserName);
			isTokenValid.set(true);
			updateActionBar();
			updateFunctions();
		}
	}

	@Override
	public void onGetError(int code, String message, Throwable err,
			Object sender) {
		setSupportProgressBarIndeterminateVisibility(false);
		if (sender == mDataService) {
			/**
			 * TODO 判断是否是服务器维护,服务端维护时返回的错误代码code:500或502
			 * 并且ShumiSdkCodeMessageBean中，
			 * 维护时返回ShumiSdkCodeMessageBean.Code:99999，
			 * 维护信息返回ShumiSdkCodeMessageBean.Message
			 */
			ShumiSdkCodeMessageBean bean = ShumiSdkApplyRecordsDataService
					.getCodeMessage(message);
			if (!TextUtils.isEmpty(bean.getMessage())) {
				Toast.makeText(this, bean.getMessage(), Toast.LENGTH_LONG)
						.show();
			} else if (code == 500 || code == 502) {
				Toast.makeText(this, R.string.server_maintaince,
						Toast.LENGTH_LONG).show();
			}
			isTokenValid.set(false);
			updateActionBar();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		this.mMenu = menu;
		updateMenu();
		return true;
	}

	protected void updateMenu() {
		if (mMenu == null) {
			return;
		}
		for (int i = 0; i < mMenu.size(); i++) {
			MenuItem mi = mMenu.getItem(i);
			mi.setTitle(ShumiSdkEnvSettings.getStr(i));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		item.setIcon(R.drawable.ic_launcher);
		if (id == R.id.update_url) {
			ShumiSdkEnv.switchToBackupUpdate(false);
		} else if (id == R.id.test_update_url) {
			ShumiSdkEnv.switchToBackupUpdate(true);
		} else if (id == R.id.online_env) {
			ShumiSdkEnv.switchToOnline();
			getAccountInfo();
		} else if (id == R.id.sandbox_env) {
			ShumiSdkEnv.switchToSandbox();
			getAccountInfo();
		} else if (id == R.id.test_env) {
			ShumiSdkEnv.switchToTesting();
			getAccountInfo();
		} else {
			IFunction func = ShumiSdkFunctionManager.get("环境\n配置");
			func.execute(this);
			return true;
		}
		updateMenu();
		ShumiSdkEnvSettings.saveDisc(this);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (System.currentTimeMillis() - mlastTime > 2000) {
				mlastTime = System.currentTimeMillis();
				Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
