package com.shumi.plugin_sdk.demo.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.Window;

import com.shumi.plugin_sdk.demo.ShumiSdkFunctionManager;
import com.shumi.plugin_sdk.demo.R;
import com.shumi.plugin_sdk.demo.bean.LocalFunction;

public class FuncActivity extends ActionBarActivity {

	public static final String FUNC = "FuncActivity";
	private LocalFunction func;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_func);
		String key = getIntent().getStringExtra(FUNC);
		func = (LocalFunction) ShumiSdkFunctionManager.get(key);
		initActionBar();
		loadFragment();
	}

	protected void initActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		if (func != null) {
			actionBar.setTitle(func.getName().replace("\n", ""));
		} else {
			finish();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void loadFragment() {
		Fragment fragment = null;
		if (func != null && (fragment = func.getFragment()) != null) {
			FragmentManager fm = getSupportFragmentManager();
			fm.beginTransaction().replace(R.id.content, fragment).commit();
		} else {
			finish();
		}
	}

}
